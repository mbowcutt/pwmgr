// (c) 2019 Michael Bowcutt <mwb71@case.edu>
// Hardware Password Manager
// CWRU EECS 396 - Independent Projects
// Prof. Chris Papachristou
// v0.8 - with Teensy-LC

#include <LiquidCrystal.h> // LCD library for UI display
#include <EEPROM.h>        // EEPROM (persistent memory) library
#include <Keyboard.h>      // Keyboard library for output

// button inputs
const int redbutton = 17;
const int bluebutton = 16;
const int greenbutton = 15;

// led ports
const int ledred = 18;
const int ledblue = 20;
const int ledgreen = 19;

// button press state
int redstate, bluestate, greenstate;

bool state; // program state
bool redup, blueup, greenup = false; // button state
int time_i; // timing variable for state switches

char pw[15] = ""; // password buffer
char title[15] = ""; // password title buffer

bool naming;  // naming substate
int pos, num; // position and number variables for naming

int entryPosition = 0;    // current selection
int maxEntryPosition = 0; // max selection

LiquidCrystal lcd(1,0,6,5,4,3);

// generate a random password
void generateNBytePassword(int n) {
  memset(pw, 0, sizeof(pw));

  for(int i = 0; i < n; i++){
    pw[i] = (char)random(32,126);     // random character
    Keyboard.write(pw[i]);            // output on creation
  }
}

// display the password title at entryPosition
void readEntryTitle() {
  memset(title, 0, sizeof(title));

  // read characters in to buffer
  for(int i = 0; i < 16; i++){
    title[i] = EEPROM.read(entryPosition + i);
  }

  // clear display and print it
  lcd.setCursor(0,1);
  lcd.print("                ");
  lcd.setCursor(0,1);
  lcd.print(title);
}

// display start/select prompt
void selectPrompt(){
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Select password:");
}

// initialize LCD to board ports

void setup() {
  Keyboard.begin();           // start keyboard
  lcd.begin(16, 2);           // start LCD
  randomSeed(analogRead(0));  // random seed for RNG

  // set buttons as input
  pinMode(redbutton, INPUT);
  pinMode(bluebutton, INPUT);
  pinMode(greenbutton, INPUT);

  // set LEDs as output
  pinMode(ledred, OUTPUT);
  pinMode(ledblue, OUTPUT);
  pinMode(ledgreen, OUTPUT);

  // activate RGB on LCD backlight
  digitalWrite(ledred, LOW);
  digitalWrite(ledblue, LOW);
  digitalWrite(ledgreen, LOW);

  // initial state = 0
  state = false;

  // update max entry position
  int val = EEPROM.read(maxEntryPosition);
  while(val != 255 && val != 0) {
    maxEntryPosition = maxEntryPosition + 32;
    val = EEPROM.read(maxEntryPosition);
  }

  // display prompt and title
  selectPrompt();
  readEntryTitle();
}

void loop() {
  // read button inputs
  redstate = digitalRead(redbutton);
  bluestate = digitalRead(bluebutton);
  greenstate = digitalRead(greenbutton);

  // if state 0, select password
  if (!state) {
    // Get button presses and times
    if (redstate == HIGH) {
      if (!redup) {
        redup = true;
        time_i = millis();
      }
    }
    else if (bluestate == HIGH) {
      if (!blueup) {
        blueup = true;
        time_i = millis();
     }
    }
    else if (greenstate == HIGH) {
      if (!greenup) {
        greenup = true;
        time_i = millis();
      }
      // if green button is held for 3s, go to state 1
      if (millis() - time_i > 3000) {
        lcd.clear();
        lcd.print("Adding new");
        lcd.setCursor(0,1);
        lcd.print("password...");
        state = true;
      }
    }
    else { // button released
      if (redup) {
        redup = false;
        // shift entry position up
        if (entryPosition == 128-32) {
          entryPosition = 0;
        }
        else {
          entryPosition += 32;
        }
        readEntryTitle();
      }
      if (blueup) {
        blueup = false;
        // shift entry position down
        if (entryPosition == 0) {
          entryPosition = 128-32;
        }
        else {
          entryPosition -= 32;
        }
        readEntryTitle();
      }
      if (greenup) {
        greenup = false;
        // write password to keyboard out
        int val;
        for (int i = 0; i < 16; i++) {
          val = EEPROM.read(entryPosition + 16 + i);
          Keyboard.write(val);
        }
      }
    }
  } // if state 0

  // if state 1, name => generate => save password
  else {

    // if green button is still being held, we're not naming yet
    if (greenup && !naming) {
      // if green button is released
      if (greenstate == LOW) {
        greenup = false;

        // begin naming
        naming = true;
        lcd.clear();
        lcd.print("Name Password:");
        num = 65;
        pos = 0;

        // prepare title buffer
        memset(title, 0, sizeof(title));
        title[pos] = (char)num;
        lcd.setCursor(0,1);
        lcd.print(title);
      }
    }

    // enter characters into title buffer
    else if (naming) {
      // get button presses

      if (redstate == HIGH) {
        if (!redup) {
          redup = true;
          time_i = millis();
        }

        // if red held for 3s, cancel button naming
        else if (millis() - time_i > 3000){
          state = false;
          selectPrompt();
        }
      }

      else if (bluestate == HIGH) {
        if (!blueup) {
          blueup = true;
          time_i = millis();
        }
        // if blue held for 3s, erase character
        else if (millis() - time_i > 3000){
          blueup = false;
          time_i = millis();
          title[pos] = ' ';
          pos--;
          if (pos < 0) {
            pos = 0;
          }
          lcd.setCursor(0,1);
          lcd.print(title);

          while (digitalRead(bluebutton) == HIGH) { }; // hold until release
        }
      }

      else if (greenstate == HIGH) {
        if(!greenup){
          greenup = true;
          time_i = millis();
        }

        // if green held for 3s, complete password
        else if (millis() - time_i > 3000){
          greenup = false;

          lcd.clear();
          lcd.setCursor(0,0);
          lcd.print("Generating");
          lcd.setCursor(0,1);
          lcd.print("password...");

          // generate password
          generateNBytePassword(16);

          // write to EEPROM
          for(int i = 0; i < 16; i++) {
            EEPROM.write(maxEntryPosition + i, title[i]);
            EEPROM.write(maxEntryPosition + 16 + i, pw[i]);
          }

          // set current entry position and increment max entry position
          entryPosition = maxEntryPosition;
          maxEntryPosition += 32;

          while (digitalRead(greenbutton) == HIGH) { }; // hold until button release

          memset(title, 0, sizeof(title));
          memset(pw, 0, sizeof(pw));

          naming = false; // done naming
          state = false; // go to state 0

          // display prompt
          selectPrompt();
          readEntryTitle();
        }
      }

      else { // button release

        if (redup){ // increment character
          redup = false;
          num++;
          if (num > 126) {
            num = 32;
          }
          title[pos] = (char)num;
          lcd.setCursor(0,1);
          lcd.print(title);
        }
        if (blueup) { // decrement character
          blueup = false;
          num--;
          if (num < 32) {
            num = 126;
          }
          title[pos] = (char)num;
          lcd.setCursor(0,1);
          lcd.print(title);
        }
        if (greenup) { // increment position
          greenup = false;
          pos++;
          if (pos > 15) {
            pos = 15;
          }
          title[pos] = (char)num;
          lcd.setCursor(0,1);
          lcd.print(title);
        }
      }
    }
  }
}
