# USB Password Manager

This *ino code generates, stores, and writes random passwords via USB keyboard output.

It was developed using a [Teensy-LC](https://www.pjrc.com/teensy/teensyLC.html) board along with an [RGB LCD screen](https://www.adafruit.com/product/398) and three momentary [buttons](https://www.adafruit.com/product/481). Additionally, a 2.2 kΩ potentiometer is needed for the LCD.

## Setup

Wire the potentiometer between 3.3v and GND.

Wire the LCD pins to the Teensy-LC

| Description | LCD Pin # | Teensy-LC Pin # |
| ------:     |      :--: | :-------:       |
| V_dd        |         2 | 5V              |
| V_ss        |         1 | Gnd             |
| V_o         |         3 | (potentiometer) |
| RS          |         4 | 0               |
| R/W         |         5 | GND             |
| EN          |         6 | 1               |
| Data[4-7]   |     11-14 | 6-3             |
| LED+        |        15 | 3.3V            |
| Red-        |        16 | 18              |
| Green-      |        17 | 19              |
| Blue-       |        18 | 20              |

Wire the buttons such that normally closed is tied to GND, normally open tied to 3.3V, and common is tied to Teensy-LC pins 15, 16, and 17.

Load the program to the board, making sure to put the device in Keyboard mode. 

## Usage

When powered, the device will prompt you to select a password. Because there are none initially, generate a new password by holding the "green" button (pin 15) for three seconds. Otherwise, you can use the red (17) or blue (16) pins to cycle right and left respectively.

Before you generate a new password, you must give it an identifier. After holding the green button for three seconds and releasing the button, you'll be prompted to enter the first character of the password name. Use the red and blue buttons to cycle up and down the ASCII map. Press the green button to add a new character, writing what's currently available. Press and hold the green button to generate and save the random password before returning to the first screen.
